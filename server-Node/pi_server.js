const http = require('http');
const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const querystring = require('querystring');

const hostname = '127.0.0.1';
const port = 3000;

const app = express();
app.use(morgan('dev'));
app.use(bodyParser.json());

app.post('/multiply', (req, res) => {
    //Setup data to send
    console.log('Stringify Data');
    const postData = JSON.parse(req.body);

    //Creating Options
    console.log('Creating Options');
    const options = {
        hostname: 'localhost',
        port: '6899',
        path: '/multiply',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': Buffer.byteLength(postData)
        }
    };

    let output = '';

    //Create HTTP Request
    console.log('Creating HTTP Request');
    const httpReq = http.request(options, (httpRes) => {
        console.log(`STATUS: ${httpRes.statusCode}`);
        console.log(`HEADERS: ${JSON.stringify(httpRes.headers)}`);

        httpRes.on('data', (chunk) => {
            console.log(`BODY: ${chunk}`);
            output += chunk;
        });
        httpRes.on('end', () => {
            console.log('No more data in response.');
            console.log('OUTPUT: ' + output);
            //Finish up POST Method
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            console.log('Sending back results: ' + output);
            res.end(output);
            console.log("FINISH");
        });
    });

    //Setup an error message
    console.log('Setup an error message');
    httpReq.on('error', (e) => {
        console.error(`problem with request: ${e.message}`);
    });

    // Write data to request body
    console.log('Writing data from http request');
    httpReq.write(postData);

    //Close HTTP Request
    console.log('Closing HTTP Request');
    httpReq.end();



});

const server = http.createServer(app);

server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});